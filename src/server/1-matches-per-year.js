const fs = require("fs");
const matches = require("../data/matches.json");

const matchesPerYear = (matches) => {
  const matchesPlayedPerYear = {};
  const outputFilePath = "../public/output/matchesPerYear.json";

  try {
    for (const match of matches) {
      matchesPlayedPerYear[match.season] = matchesPlayedPerYear[match.season]
        ? (matchesPlayedPerYear[match.season] += 1)
        : 1;
    }

    const writeStream = fs.createWriteStream(outputFilePath);
    writeStream.write(JSON.stringify(matchesPlayedPerYear, null, 2));
    writeStream.end();

    writeStream.on("finish", () => {
      console.log("Output file created successfully");
    });

    writeStream.on("error", (err) => {
      throw err; // Pass the error to the catch block
    });
  } catch (error) {
    console.log("An error occurred during processing:", error.message);
  } finally {
    console.log("Processing completed.");
  }
};

matchesPerYear(matches);